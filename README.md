(I need to rotate the sprites and fix a bug or two. It plays quite weirdly atm. The fixes are very simple (eg rotate the sprites), I'll do it when I have time

# TankGame
A small (classic) Grand Theft Auto-style game that I've wanted to make for *years*. (I try to work on it whenever I have time, so I encourage you to give it a play :)

## Controls:
* W: Move forward, S: Move backward, A: Turn Left, D: Turn Right
* Mouse: Aim the weapon (Left click: Fire primary weapon), Right click: Secondary Weapon)
* Q/E: Use left/right vehicle signals)
* Enter: Enter (or exit a vehicle)
* Tab: Horn/sieren
* Space: Handbrake

## Features:
* Player able to take control of any of multiple vehicles (Tank, sports car) with very different handling and features
(more coming, see TODO)

## Learning Objects/Goals:
* Get comfortable/productive with python
* Learn pygame's API and principles (pygame is a python wrapper for [SDL](http://en.wikipedia.org/wiki/Simple_DirectMedia_Layer), used in some popular commercial games)
* Use socket programming to implement robust and low latency network play (a real-world scenario)
* Rapidly implementing standard algorithms (AI searching, ) from scratch for practise/instant recall of inner workings (rather than relying on library functions)
* Get in the habit of using my free time to work on side projects whenever I get an idea that sounds interesting. (which feeds back to help generation of more ideas)
(I hope by explicitly defining learning objective will make my progress easily measurable)

## TODO:
* Finish implementing primary controls (vehicle exit/entering)
* Vehicles AI
* Display health/ammo/armour/wanted info on screen
* Create a few levels :)
* Network Play (yay!)
* Polish and declare project done (well.. At least for now :)

## History
I actually began working on a similar game in primary school using [Mark Overmars](http://en.wikipedia.org/wiki/Mark_Overmars)' amazing [GameMaker](http://yoyogames.com). Regretably I stopped experimenting with programming side-projects and games once I reached middle school. I only started programming again in force after entering university, so now it's a good time to revisit all the old ideas I've always wanted implement, do a heap of side-projects and further develop my programming (and rapid prototyping) skills! :) This is step 1: To get away from C/C++/Java mindset with a few quick couple-thousand line programs in python (and pygame) ;)

## Screenshot
![What a beautifully drawn Tank!](https://dl.dropboxusercontent.com/u/6634730/github/TankGame/TankGameScreenshot.jpg "TankGame screenshot")
(The dot is the human player who enter either vehicle and drive it around. The tank gun aims towards the mouse cursor, but it's not rotating around the central point yet)
