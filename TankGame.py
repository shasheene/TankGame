#/usr/bin/python
"""
sediriw 2013-02-15

Topdown tank game
"""

import os, sys,math
import pygame, string
from pygame.locals import *

if not pygame.mixer: print 'Warning, sound disabled'

def load_image(name,colorkey=None):
    fullname = os.path.join('sprites',name)
    try:
        image = pygame.image.load(fullname)
    except pygame.error, message:
        print 'Cannot load image:', name
        raise SystemExit, message
    image = image.convert()
    if colorkey is not None:
        if colorkey is -1:
            colorkey = image.get_at((0,0))
        image.set_colorkey(colorkey, RLEACCEL)
    return image, image.get_rect()

def load_sound(name):
    class NoneSounde:
        def play(self): pass
    if not pygame.mixer:
        return NoneSound()
    fullname = os.path.join('sounds', name)
    try:
        sound = pygame.mixer.Sound(fullname)
    except pygame.error, message:
        print 'Cannot load sound:', wav
        raise SystemExit, message
    return sound

#All user controlled entities (cars, tanks, boats, human) are derived from this Vehicle class
class Vehicle(pygame.sprite.Sprite):
    def __init__(self,vehicleSpec,spawnPoint,friction=1):
        pygame.sprite.Sprite.__init__(self)#Sprite initializer
        self.image,self.rect = load_image(vehicleSpec['spritePath'],-1)
        self.rect = self.rect.move(spawnPoint)
        self.friction = friction
        self.original = self.image  #Backup of original image

        self.accelerationRate = vehicleSpec['accelRate']
        self.deccelerationRate = vehicleSpec['decelRate']
        self.turningSpeed = vehicleSpec['turningSpeed'] #Threshold until vehicle turns. Low = fast
        self.topSpeed = vehicleSpec['topSpeed']
        
        #Fixed vehicle attributes
        self.numAnimations = 100 #Animation every 3.6 degrees
        self.speed = 0
        self.direction = 0 #Direction facing
        self.lTurn = 0
        self.rTurn = 0
        

    def update(self):
        if self.speed>0:
            if self.friction==1:
                self.speed-=0.1 #Friction
            self.x = math.cos(math.radians(self.direction))*self.speed
            self.y = math.sin(math.radians(self.direction))*self.speed
            self.rect.center = (self.rect.center[0]+self.x,self.rect.center[1]+self.y)

            center = self.rect.center
            rotate = pygame.transform.rotate #In pygame negative rotates clockwise
            self.image = rotate(self.original, -self.direction)
            self.rect = self.image.get_rect(center=center)
            
            #If a player leaves the screen they come back from the other side
            if self.rect.center[0] > 800:
                self.rect.center = (0,800-self.rect.center[1])
            elif self.rect.center[0] < 0:
                self.rect.center = (800,self.rect.center[1])
            if self.rect.center[1] > 800:
                self.rect.center = (self.rect.center[0],0)
            elif self.rect.center[1] < 0:
                self.rect.center = (self.rect.center[0],800)
                
    def accelerate(self):
        if self.speed<self.topSpeed:
            self.speed+=self.accelerationRate
    def deccelerate(self):
        self.speed-=self.deccelerationRate
    def turn_left(self):
        self.lTurn+=self.turningSpeed
        if self.lTurn > self.turningSpeed:
            self.direction-=360/100;
            self.lTurn = 0
    def turn_right(self):
        self.rTurn+=self.turningSpeed
        if self.rTurn > self.turningSpeed:
            self.direction+=360/100;
            self.rTurn = 0
    def enterleave_vehicle(self):
        global inControl
        global gameObjects
        collisions = self.rect.collidelistall(gameObjects)
        if inControl == 0: #if we are not player
            if len(collisions)>1:
                if collisions[0]==0:
                    inControl = collisions[1]
                    print 'Switching to ',collisions
        else:
            gameObjects[0].rect.center = (self.rect.center[0]+50,self.rect.center[1]+50)
            inControl = 0
            print 'Switching to player',collisions
    def fire(self):
        pass #Most vehicles can't fire

class Player(Vehicle):
    def __init__(self,spawnPoint):
        specs = {'spritePath':'sprPlayer.png','topSpeed':2,'accelRate':2,'decelRate':0.2,
                 'turningSpeed':0.05}
        super(Player, self).__init__(specs, spawnPoint,1) #No friction for player

class TankBase(Vehicle):
    def __init__(self,spawnPoint):
        specs = {'spritePath':'sprTankBase.png','topSpeed':3,'accelRate':1,'decelRate':1,
                 'turningSpeed':0.1 }
        super(TankBase, self).__init__(specs, spawnPoint)
        self.gun = TankGun(self)
    def update(self):
        super(TankBase, self).update()
        self.gun.update()
    def fire(self):
        self.gun.fire()

class SportsCar(Vehicle):
    def __init__(self,spawnPoint):
        specs = {'spritePath':'sprSportsCar.png','topSpeed':8,'accelRate':1,'decelRate':1,
                 'turningSpeed':0.3}
        super(SportsCar, self).__init__(specs, spawnPoint)

class TankGun(pygame.sprite.Sprite):
    def __init__(self, tank):
        #pivot over 39,40 on big image
        self.direction = 0 #Direction facing
        pygame.sprite.Sprite.__init__(self)#Sprite initializer
        self.image,self.rect = load_image('sprTankGun.png',-1)
        
        self.rect.center = (26,25)
        self.rect.inflate(400,400)

        self.original = self.image  #Backup of original image
        self.tank = tank
    def update(self):
        self.mouse_pos = pygame.mouse.get_pos()

        # Look over below section again.
        # In pygame (as in Maths( positive floats rotate anti-clockwise.
        
        self.temp_y_calc = self.rect.center[1] - self.mouse_pos[1] #Mouse y offset
        self.temp_x_calc = self.rect.center[0] - self.mouse_pos[0] #Mouse x offset
        self.temp_dir_radian = math.atan2(self.temp_y_calc,self.temp_x_calc) #Angle to mouse
        self.direction= -math.degrees(self.temp_dir_radian) + 180

        #In pygame positive floats rotate anti-clockwise.
        #Correction required due to python.math/pygame not agreeing
        #0 degrees faces up (in pygame) or faces right (mathematics)?

        rotate = pygame.transform.rotate
        self.image = rotate(self.original, self.direction)
        rotate = pygame.transform.rotate
        self.image = rotate(self.original, self.direction)
        
        self.rect.center = self.tank.rect.center
        

    def fire(self):
        self.temp_y_calc = self.rect.center[1] - self.mouse_pos[1] #Mouse y offset
        self.temp_x_calc = self.rect.center[0] - self.mouse_pos[0] #Mouse x offset
        self.norm_x = self.temp_x_calc/(self.temp_x_calc+self.temp_y_calc)
        self.norm_y = self.temp_x_calc/(self.temp_x_calc+self.temp_y_calc)
        print 'fire'
        bullet = TankBullet(3,(self.norm_x,self.norm_y))
    def accelerate(self):
        pass
    def deccelerate(self):
        pass
    def turn_left(self):
        pass
    def turn_right(self):
        pass



class TankBullet(pygame.sprite.Sprite):
    def __init__(self, speed, slope):
        pygame.sprite.Sprite.__init__(self)#Sprite initializer
        self.image,self.rect = load_image('sprTankBullet.png',-1)
        global gameObjects
        gameObjects.append(self)
        self.speed = speed
    def update(self):
        self.rect.move(self.slope*self.speed)
        if self.rect.center[0] > 800 or self.rect.center[0] < 0 or  self.rect.center[1] > 800 or self.rect.center[1] < 0:
            #gameObjects.remove(self)
            print 'died!'

def main():
    """this function is called when the program starts.
                it initializes everything it needs, then runs in
       a loop until the function returns."""

#Initialize Everything
    pygame.init()
    screen = pygame.display.set_mode((800,800))
    global screenDimensions
    screenDimensions = screen.get_size()
    pygame.display.set_caption('TankGame')
    
    pygame.mouse.set_visible(0)

#Create The Backgound
    background = pygame.Surface(screenDimensions)
    background = background.convert()
    background.fill((165, 165, 165))

#Display The Background
    screen.blit(background, (0, 0))
    pygame.display.flip()

#Prepare Game Objects
    clock = pygame.time.Clock()

    tank = TankBase((250,400))
    sportscar = SportsCar((100,100))
    player = Player((500,500))
    global gameObjects
    gameObjects = [player,tank,sportscar]
    global inControl
    inControl = 0 #index of above.


    #seperateMap file 
    #f = open('levels/map01.txt','r')
    #map_line = f.readlines()
    #while True:
     #   if len(map_line)==0: break
      #  form = string.Formatter
       # coord = form.parse(map_line)
        #gameObjects.append(SportsCar(coord[0]))
           
    allsprites = pygame.sprite.OrderedUpdates(gameObjects,tank.gun)


#Main Loop
    while 1:
        clock.tick(60)
    #Handle Input Events
        pygame.event.pump()
        key=pygame.key.get_pressed()
    #if key[pygame.K_q]:  = 0
        #if key[pygame.K_e]:  = 1
        
        if key[pygame.K_w]: gameObjects[inControl].accelerate()
        if key[pygame.K_s]: gameObjects[inControl].deccelerate()
        if key[pygame.K_a]: gameObjects[inControl].turn_left()
        if key[pygame.K_d]: gameObjects[inControl].turn_right()
        
        if (key[pygame.K_LCTRL] and key[pygame.K_c]) or key[pygame.K_ESCAPE]: exit()
        if key[pygame.K_RIGHTBRACKET]:
            python = sys.executable
            os.execl(python, python, * sys.argv)
        
        for event in pygame.event.get():
            if event.type == KEYDOWN and event.key == K_RETURN:
                gameObjects[inControl].enterleave_vehicle()
            if event.type == MOUSEBUTTONDOWN:
                gameObjects[inControl].fire()
        

        tank.update()
        #gun.update()
        sportscar.update()
        player.update()
    #Draw Everything
        screen.blit(background, (0, 0))
        allsprites.draw(screen)
        pygame.display.flip()


#Game Over


#this calls the 'main' function when this script is executed
if __name__ == '__main__': main()
